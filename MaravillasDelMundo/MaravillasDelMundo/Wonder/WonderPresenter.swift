//
//  WonderPresenter.swift
//  MaravillasDelMundo
//
//  Created by Gerardo Naranjo on 28/03/22.
//

import Foundation

// MARK: Protocol
protocol AnyPresenter {
    var view: AnyView? { get set }
    var router: AnyRouter? { get set }
    var interactor: AnyInteractor? { get set }
    func interactorSetWonders(with result: Result<[WonderEntity], Error>)
}

// MARK: Class
class WonderPresenter: AnyPresenter {

    /// View.
    var view: AnyView?

    /// Router.
    var router: AnyRouter?

    /// Interactor.
    var interactor: AnyInteractor? {
        didSet {
            interactor?.setSevenWonders()
        }
    }

    /// Function that will be called from the Interactor when the wonders are finished configuring.
    /// - Parameter result: It will determine whether or not there was an error during setup.
    func interactorSetWonders(with result: Result<[WonderEntity], Error>) {
        switch result {
        case .success(let wonders):
            view?.setWonders(wonders: wonders)
        default:
            view?.setWonders(errorMessage: "Error")
        }
    }
}

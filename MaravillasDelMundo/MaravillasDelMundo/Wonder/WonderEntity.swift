//
//  WonderEntity.swift
//  MaravillasDelMundo
//
//  Created by Gerardo Naranjo on 28/03/22.
//

import Foundation

// MARK: Struct
struct WonderEntity {
    let title: String       // name for the wondder
    let description: String
}

//
//  Router.swift
//  MaravillasDelMundo
//
//  Created by Gerardo Naranjo on 28/03/22.
//

import UIKit

typealias EntryPoint = AnyView & UIViewController

// MARK: Protocol
protocol AnyRouter {
    var entry: EntryPoint? { get set }
    static func start() -> AnyRouter
}

// MARK: Class
class WonderRouter: AnyRouter {

    /// Variable for entry point of the view.
    var entry: EntryPoint?

    /// Function that will be called to initialize the Router.
    /// - Returns: Router.
    static func start() -> AnyRouter {
        let router = WonderRouter()

        var view: AnyView = WonderView()
        var presenter: AnyPresenter = WonderPresenter()
        var interactor: AnyInteractor = WonderInteractor()

        view.presenter = presenter

        interactor.presenter = presenter

        presenter.router = router
        presenter.view = view
        presenter.interactor = interactor

        router.entry = view as? EntryPoint

        return router
    }

}

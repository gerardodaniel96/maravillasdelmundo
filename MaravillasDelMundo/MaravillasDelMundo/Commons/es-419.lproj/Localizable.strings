/* 
  Localizable.strings
  MaravillasDelMundo

  Created by Gerardo Naranjo on 28/03/22.
  
*/

"ib.WWWonderInteractor.tableViewCellText.firstWonder" = "Pirámide de Chichén Itzá en Chichén Itzá, península de Yucatán, México";
"ib.WWWonderInteractor.tableViewCellSecondaryText.firstWonder" = "El Castillo, también conocido como el Templo de Kukulcán, es una pirámide escalonada mesoamericana que domina el centro del sitio arqueológico de Chichén Itzá en el estado mexicano de Yucatán. El edificio está designado más formalmente por los arqueólogos como Chichén Itzá Estructura 5B18.

    Construido por la civilización maya precolombina en algún momento entre los siglos IX y XII d.C., El Castillo sirvió como templo al dios Kukulkán, la deidad de la serpiente emplumada maya yucateca estrechamente relacionada con el dios Quetzalcóatl conocido por los aztecas y otras culturas del centro de México. el período Posclásico.

    La pirámide consta de una serie de terrazas cuadradas con escaleras en cada uno de los cuatro lados hasta el templo en la parte superior. Esculturas de serpientes emplumadas corren por los lados de la balaustrada norte. Durante los equinoccios de primavera y otoño, el sol de la tarde incide en la esquina noroeste de la pirámide y proyecta una serie de sombras triangulares contra la balaustrada del noroeste, creando la ilusión de una serpiente emplumada “arrastrándose” por la pirámide. El evento ha sido muy popular, pero es cuestionable si es el resultado de un diseño intencionado. Cada uno de los cuatro lados de la pirámide tiene 91 escalones que, cuando se suman e incluyen la plataforma del templo en la parte superior como el “escalón” final, producen un total de 365 escalones (que es igual al número de días del año Haab).

    La estructura tiene 24 m (79 pies) de altura, más 6 m (20 pies) adicionales para el templo. La base cuadrada mide 55,3 m (181 pies) de ancho.

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.secondWonder" = "Cristo Redentor, Río de Janeiro, Brasil";
"ib.WWWonderInteractor.tableViewCellSecondaryText.secondWonder" = "El Cristo Redentor es una estatua Art Deco de Jesucristo en Río de Janeiro, Brasil, creada por el escultor polaco-francés Paul Landowski y construida por el ingeniero brasileño Heitor da Silva Costa, en colaboración con el ingeniero francés Albert Caquot. El rostro fue creado por el artista rumano Gheorghe Leonida. La estatua mide 30 metros (98 pies) de alto, sin incluir su pedestal de 8 metros (26 pies), y sus brazos se extienden 28 metros (92 pies) de ancho. En comparación, es aproximadamente dos tercios de la altura de la Estatua de la Libertad desde la base hasta la antorcha.

    La estatua pesa 635 toneladas métricas (625 largas, 700 toneladas cortas) y está ubicada en la cima de la montaña Corcovado de 700 metros (2300 pies) en el Parque Nacional del Bosque de Tijuca con vista a la ciudad de Río. Símbolo del cristianismo en todo el mundo, la estatua también se ha convertido en un icono cultural tanto de Río de Janeiro como de Brasil, y figura como una de las Nuevas Siete Maravillas del Mundo. Está hecho de hormigón armado y esteatita, y fue construido entre 1922 y 1931.

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.thirdWonder" = "Machu Picchu, Región Cuzco, Perú";
"ib.WWWonderInteractor.tableViewCellSecondaryText.thirdWonder" = "Machu Picchu, es una ciudadela inca del siglo XV situada en una cordillera de 2.430 metros (7.970 pies) sobre el nivel del mar. Está ubicado en la Región Cusco, Provincia de Urubamba, Distrito de Machupicchu en Perú, sobre el Valle Sagrado, que se encuentra a 80 kilómetros (50 millas) al noroeste de Cuzco y a través del cual fluye el río Urubamba.

    La mayoría de los arqueólogos creen que Machu Picchu fue construido como propiedad del emperador inca Pachacuti (1438-1472). A menudo denominada erróneamente como la \"Ciudad Perdida de los Incas\" (un título que se aplica con mayor precisión a Vilcabamba), es el ícono más familiar de la civilización Inca. Los incas construyeron la hacienda alrededor de 1450 pero la abandonaron un siglo después en la época de la conquista española. Aunque conocido localmente, los españoles no lo conocían durante el período colonial y permaneció desconocido para el mundo exterior hasta que el historiador estadounidense Hiram Bingham lo llamó la atención internacional en 1911.

    Machu Picchu fue construido en el estilo Inca clásico, con paredes de piedra seca pulida. Sus tres estructuras principales son el Inti Watana, el Templo del Sol y la Sala de las Tres Ventanas. La mayoría de los edificios periféricos han sido reconstruidos para dar a los turistas una mejor idea de cómo eran originalmente. Para 1976, el treinta por ciento de Machu Picchu había sido restaurado y la restauración continúa.

    Machu Picchu fue declarado Santuario Histórico del Perú en 1981 y Patrimonio de la Humanidad por la UNESCO en 1983. En 2007, Machu Picchu fue votada como una de las Nuevas Siete Maravillas del Mundo en una encuesta mundial por Internet.

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.fourthWonder" = "Taj Mahal, Agra y Uttar Pradesh, India";
"ib.WWWonderInteractor.tableViewCellSecondaryText.fourthWonder" = "El Taj Mahal es un mausoleo de mármol blanco marfil en la orilla sur del río Yamuna en la ciudad india de Agra. En 2007, fue declarado ganador de la iniciativa New7Wonders of the World (2000-2007).

    Fue encargado en 1632 por el emperador mogol, Shah Jahan (que reinó entre 1628 y 1658), para albergar la tumba de su esposa favorita, Mumtaz Mahal. La tumba es la pieza central de un complejo de 42 acres, que incluye una mezquita y una casa de huéspedes, y está ubicada en jardines formales delimitados en tres lados por un muro almenado.

    La construcción del mausoleo se completó esencialmente en 1643, pero el trabajo continuó en otras fases del proyecto durante otros 10 años. Se cree que el complejo Taj Mahal se completó en su totalidad en 1653 a un costo estimado en ese momento de alrededor de 32 millones de rupias, que en 2015 serían aproximadamente 52,8 mil millones de rupias (US $ 827 millones). El proyecto de construcción empleó a unos 20.000 artesanos bajo la dirección de una junta de arquitectos dirigida por el arquitecto de la corte del emperador, Ustad Ahmad Lahauri.

    El Taj Mahal fue designado como Patrimonio de la Humanidad por la UNESCO en 1983 por ser \"la joya del arte musulmán en la India y una de las obras maestras universalmente admiradas del patrimonio mundial\". Descrito por el premio Nobel Rabindranath Tagore como \"la lágrima en la mejilla del tiempo\", muchos lo consideran el mejor ejemplo de la arquitectura mogol y un símbolo de la rica historia de la India. El Taj Mahal atrae entre 7 y 8 millones de visitantes al año.

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.fifthWonder" = "Petra, Gobernación de Ma'an, Jordania";
"ib.WWWonderInteractor.tableViewCellSecondaryText.fifthWonder" = "Petra, originalmente conocida por los nabateos como Raqmu, es una ciudad histórica y arqueológica en el sur de Jordania. La ciudad es famosa por su arquitectura excavada en la roca y su sistema de conductos de agua. Otro nombre para Petra es la Ciudad Rosa debido al color de la piedra en la que está tallada.

    Establecida posiblemente ya en el año 312 a. C. como la capital de los árabes nabateos, es un símbolo de Jordania, así como la atracción turística más visitada de Jordania. Los nabateos eran árabes nómadas que se beneficiaron de la proximidad de Petra a las rutas comerciales regionales, al convertirse en un importante centro comercial, lo que les permitió acumular riqueza. Los nabateos también son conocidos por su gran habilidad para construir métodos eficientes de recolección de agua en los áridos desiertos y su talento para tallar estructuras en rocas sólidas. Se encuentra en la ladera de Jebel al-Madhbah (identificado por algunos como el Monte Hor bíblico) en una cuenca entre las montañas que forman el flanco oriental de Arabah (Wadi Araba), el gran valle que se extiende desde el Mar Muerto hasta el Golfo de Áqaba. Petra ha sido Patrimonio de la Humanidad por la UNESCO desde 1985.

    El sitio permaneció desconocido para el mundo occidental hasta 1812, cuando fue presentado por el explorador suizo Johann Ludwig Burckhardt. Se describió como \"una ciudad rosa roja la mitad de antigua que el tiempo\" en un poema ganador del premio Newdigate de John William Burgon. La UNESCO lo ha descrito como “uno de los bienes culturales más preciados del patrimonio cultural del hombre”. Petra fue nombrada entre las Nuevas 7 Maravillas del Mundo en 2007 y también fue elegida por la revista Smithsonian como uno de los \"28 lugares para ver antes de morir\".

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.sixthWonder" = "Coliseo, Roma, Italia";
"ib.WWWonderInteractor.tableViewCellSecondaryText.sixthWonder" = "El Coliseo o Coliseo, también conocido como Anfiteatro Flavio, es un anfiteatro ovalado en el centro de la ciudad de Roma, Italia. Construido de hormigón y arena, es el anfiteatro más grande jamás construido. El Coliseo está situado justo al este del Foro Romano. La construcción comenzó bajo el emperador Vespasiano en el año 72 d. C. y se completó en el 80 d. C. bajo su sucesor y heredero Tito. Se hicieron más modificaciones durante el reinado de Domiciano (81-96). Estos tres emperadores se conocen como la dinastía Flavia, y el anfiteatro recibió su nombre en latín por su asociación con su apellido (Flavius).

    El Coliseo podría albergar, se estima, entre 50.000 y 80.000 espectadores, teniendo una audiencia media de unos 65.000; se usó para concursos de gladiadores y espectáculos públicos como simulacros de batallas navales (solo por un corto tiempo ya que el hipogeo pronto se llenó con mecanismos para apoyar las otras actividades), cacerías de animales, ejecuciones, recreaciones de batallas famosas y dramas. basado en la mitología clásica. El edificio dejó de utilizarse para el entretenimiento en la época medieval temprana. Posteriormente se reutilizó para vivienda, talleres, cuartel de una orden religiosa, fortaleza, cantera y santuario cristiano.

    Aunque parcialmente arruinado por los daños causados ​​por los terremotos y los ladrones de piedras, el Coliseo sigue siendo un símbolo icónico de la Roma imperial. Es una de las atracciones turísticas más populares de Roma y también tiene vínculos con la Iglesia Católica Romana, ya que cada Viernes Santo el Papa encabeza una procesión del \"Vía Crucis\" iluminada con antorchas que comienza en el área alrededor del Coliseo.

    El Coliseo también está representado en la versión italiana de la moneda de cinco céntimos de euro.

    El nombre latino original del Coliseo era Amphitheatrum Flavium, a menudo anglicismo como Anfiteatro Flavio. El edificio fue construido por emperadores de la dinastía Flavia, siguiendo el reinado de Nerón. Este nombre todavía se usa en inglés moderno, pero generalmente la estructura se conoce mejor como el Coliseo. En la antigüedad, los romanos pueden haberse referido al Coliseo con el nombre no oficial Amphitheatrum Caesareum (con Caesareum un adjetivo perteneciente al título César), pero este nombre puede haber sido estrictamente poético ya que no era exclusivo del Coliseo; Vespasiano y Tito, constructores del Coliseo, también construyeron un anfiteatro del mismo nombre en Puteoli (actual Pozzuoli).

    Durante mucho tiempo se ha creído que el nombre Coliseo se deriva de una estatua colosal de Nerón cercana (la estatua de Nerón lleva el nombre del Coloso de Rodas). Esta estatua fue posteriormente remodelada por los sucesores de Nerón a semejanza de Helios (Sol) o Apolo, el dios del sol, al agregar la corona solar apropiada. La cabeza de Nerón también fue reemplazada varias veces por las cabezas de los emperadores sucesivos. A pesar de sus vínculos paganos, la estatua se mantuvo en pie hasta bien entrada la era medieval y se le atribuyeron poderes mágicos. Llegó a ser visto como un símbolo icónico de la permanencia de Roma.

    En el siglo VIII, un famoso epigrama atribuido al Venerable Beda celebró el significado simbólico de la estatua en una profecía que se cita de diversas formas: Quamdiu stat Colisæus, stat et Roma; quando cadete colisæus, cadete et Roma; quando cadet Roma, cadet et mundus (“mientras subsista el Coloso, así permanecerá Roma; cuando caiga el Coloso, caerá Roma; cuando caiga Roma, caerá el mundo”). Esto a menudo se traduce mal para referirse al Coliseo en lugar del Coloso (como, por ejemplo, en el poema de Byron Childe Harold's Pilgrimage). Sin embargo, en el momento en que escribió Pseudo-Bede, el sustantivo masculino coliseus se aplicó a la estatua en lugar de lo que todavía se conocía como el anfiteatro Flavio.

    El Coloso finalmente cayó, posiblemente siendo derribado para reutilizar su bronce. Para el año 1000 se acuñó el nombre “Coliseo” para referirse al anfiteatro. La estatua en sí fue olvidada en gran parte y solo sobrevive su base, situada entre el Coliseo y el cercano Templo de Venus y Roma.

    El nombre evolucionó aún más a Coliseum durante la Edad Media. En Italia, el anfiteatro todavía se conoce como il Colosseo, y otras lenguas romances han llegado a utilizar formas similares como Coloseumul (rumano), le Colisée (francés), el Coliseo (español) y o Coliseu (portugués).

    - https://world.new7wonders.com";

"ib.WWWonderInteractor.tableViewCellText.seventhWonder" = "La Gran Muralla China";
"ib.WWWonderInteractor.tableViewCellSecondaryText.seventhWonder" = "La Gran Muralla China es una serie de fortificaciones hechas de piedra, ladrillo, tierra apisonada, madera y otros materiales, generalmente construidas a lo largo de una línea de este a oeste a lo largo de las fronteras históricas del norte de China para proteger a los estados e imperios chinos contra las incursiones e invasiones de los diversos grupos nómadas de la estepa euroasiática. Se estaban construyendo varios muros ya en el siglo VII a. C.; estos, luego unidos y hechos más grandes y fuertes, ahora se conocen colectivamente como la Gran Muralla. Especialmente famoso es el muro construido entre el 220 y el 206 a. C. por Qin Shi Huang, el primer emperador de China. Queda poco de ese muro. Desde entonces, la Gran Muralla ha sido reconstruida, mantenida y mejorada de vez en cuando; la mayor parte del muro existente es de la dinastía Ming.

    Otros propósitos de la Gran Muralla han incluido los controles fronterizos, que permiten la imposición de aranceles sobre los bienes transportados a lo largo de la Ruta de la Seda, la regulación o fomento del comercio y el control de la inmigración y la emigración. Además, las características defensivas de la Gran Muralla se vieron reforzadas por la construcción de torres de vigilancia, cuarteles de tropas, estaciones de guarnición, capacidades de señalización a través de humo o fuego, y el hecho de que el camino de la Gran Muralla también servía como corredor de transporte. .

    La Gran Muralla se extiende desde Dandong en el este hasta el lago Lop en el oeste, a lo largo de un arco que delinea aproximadamente el borde sur de Mongolia Interior. Un estudio arqueológico exhaustivo, que utilizó tecnologías avanzadas, concluyó que los muros Ming miden 8.850 km (5.500 millas). Esto se compone de 6.259 km (3.889 millas) de secciones de muro real, 359 km (223 millas) de trincheras y 2.232 km (1.387 millas) de barreras defensivas naturales, como colinas y ríos. Otro estudio arqueológico encontró que todo el muro con todas sus ramas mide 21.196 km (13.171 millas).

    - https://world.new7wonders.com";
